package com.example.boosters;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class soporte extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soporte);
    }
    public void Cancelar(View v){
        finish();
    }

    public void Enviar(View v){

        eleccion("Su solicitud fue generada exitosamente. Nos pondremos en contacto a la brevedad posible.");

    }


    public void eleccion(String cadena) {
        //se prepara la alerta creando nueva instancia
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        //seleccionamos la cadena a mostrar
        alertbox.setMessage(cadena);
        //elegimos un positivo SI y creamos un Listener
        alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            //Funcion llamada cuando se pulsa el boton Si
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });

        //elegimos un positivo NO y creamos un Listener
                /*   alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            //Funcion llamada cuando se pulsa el boton No
            public void onClick(DialogInterface arg0, int arg1) {
                mensaje("Pulsado el botón NO");
            }
        });*/

        //mostramos el alertbox
        alertbox.show();
    }


}