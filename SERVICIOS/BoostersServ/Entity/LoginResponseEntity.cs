﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoostersRestApi.Entidades
{
    public class LoginResponseEntity
    {

        public string Usuario { set; get; }
        public string Nombre { set; get; }
        public string Apellido { set; get; }

    }
}
