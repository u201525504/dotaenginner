﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoostersRestApi.Entidades
{
    public class EUsuarioEntity
    {
        public string Nombre { set; get; }
        public string Apellido { set; get; }
        public string Dni { set; get; }
        public string Correo { set; get; }
        public string Usuario { set; get; }
        public string Clave { set; get; }
        public string Alias { set; get; }
        public string EsBooster { set; get; }
        public string IdSteam { set; get; }


    }
}
