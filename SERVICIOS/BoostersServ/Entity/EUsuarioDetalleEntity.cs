﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoostersRestApi.Entidades
{

    public class EUsuarioDetalleEntity
    {
        public int Codigo { set; get; }
        public string Nombre { set; get; }
        public string Apellido { set; get; }
        public string Dni { set; get; }
        public string Contraseña { set; get; }
        public string IdSteam { set; get; }
        public string Telefono { set; get; }
        public string Ubicacion { set; get; }

        public string medalla { set; get; }
        public string Correo { set; get; }

    }
}
