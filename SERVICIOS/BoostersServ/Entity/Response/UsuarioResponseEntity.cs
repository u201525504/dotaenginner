﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoostersRestApi.Entidades
{
    public class UsuarioResponseEntity
    {
        public int Estado { set; get; }
        public int codUsuario { set; get; }
        public string EsBooster { set; get; }
        public string Alias { set; get; }
        public string Nombre { set; get; }
        public string Apellido { set; get; }
        public string Dni { set; get; }
        public string CorreoRegistro { set; get; }
        public string idSteam { set; get; }
        public string Medalla { set; get; }
        public string Calificacion { set; get; }
        public string Telefono { set; get; }
        public string CorreoContacto { set; get; }
        public string Ubicacion { set; get; }
    }
}
