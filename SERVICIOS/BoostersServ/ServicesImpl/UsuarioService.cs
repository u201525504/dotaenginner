﻿using BoostersRestApi.Entidades;
using BoostersRestApi.Data;
using BoostersServ.Service;
using Integracion.Interface;
using BoostersServ;
using System;
using System.Collections.Generic;

namespace BoostersRestApi.Service
{
    public class UsuarioService : IUsuarioService   
    {
        
        private readonly IUsuarioDAO UsuarioDAO;

        public UsuarioService(IUsuarioDAO UsuarioDAO)
        {
            this.UsuarioDAO = UsuarioDAO;
        }

        public int BoosterDetalle(EUsuarioDetalleEntity entity)
        {
            return UsuarioDAO.InsBoosterDetalle(entity);
        }

        public List<UsuarioResponseEntity> BusquedaBooster()
        {
            return UsuarioDAO.BusquedaBooster();
        }

        public LoginResponseEntity ObtenerDatosLoginRespuesta(ELoginEntity entity)
        {
            LoginResponseEntity entityResult = null;
            
            entityResult = UsuarioDAO.ObtenerDatos(entity);

            return entityResult;
        }

        public UsuarioResponseEntity RegistroUsuario(EUsuarioEntity entity)
        {
            UsuarioResponseEntity entityResult = null;

            entityResult = UsuarioDAO.Registro(entity);

            return entityResult;
        }


        public UsuarioResponseEntity BusquedaBoosterById(int id)
        {
            return UsuarioDAO.BusquedaBoosterById(id);
        }

    }
}
