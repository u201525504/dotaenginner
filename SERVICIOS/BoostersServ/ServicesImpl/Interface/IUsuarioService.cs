﻿using BoostersRestApi.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoostersServ.Service
{
    public interface IUsuarioService
    {
        LoginResponseEntity ObtenerDatosLoginRespuesta(ELoginEntity entity);
        UsuarioResponseEntity RegistroUsuario(EUsuarioEntity entity);
        int BoosterDetalle(EUsuarioDetalleEntity entity);
        List<UsuarioResponseEntity> BusquedaBooster();
        UsuarioResponseEntity BusquedaBoosterById(int id);
    }
}
