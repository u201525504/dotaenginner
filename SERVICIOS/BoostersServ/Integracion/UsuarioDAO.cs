﻿using BoostersRestApi.Entidades;
using Integracion;
using Integracion.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BoostersRestApi.Data
{
    public class UsuarioDAO : IUsuarioDAO
    {
        private readonly LoginResponseEntity loginResponseEntity;
        private readonly UsuarioResponseEntity usuarioResponseEntity;
        protected string _conexion;


        public UsuarioDAO(LoginResponseEntity loginResponseEntity, UsuarioResponseEntity usuarioResponseEntity)
        {
            _conexion = Conexion.GetConectionString(Constantes.CNXBOOSTER);




            this.loginResponseEntity = loginResponseEntity;
            this.usuarioResponseEntity = usuarioResponseEntity;
        }

        public LoginResponseEntity ObtenerDatos(ELoginEntity entity)
        {
            LoginResponseEntity usuarioEntity = new LoginResponseEntity();



            using (SqlConnection cn = new SqlConnection(_conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SP_VAL_USUARIOINICIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Usuario", entity.Usuario);
                    cmd.Parameters.AddWithValue("@Contraseña", entity.Password);
                    cn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        usuarioEntity.Codigo = reader["co_cliente"].ToString();
                        usuarioEntity.Usuario = reader["tx_usuario"].ToString();
                        usuarioEntity.Nombre = reader["no_nombre"].ToString();
                        usuarioEntity.Apellido = reader["no_apellido"].ToString();
                        usuarioEntity.Tipo = reader["co_tipo"].ToString();
                    }
                }
            }


            return usuarioEntity;

        }
        public UsuarioResponseEntity Registro(EUsuarioEntity entity)
        {
            using (SqlConnection cn = new SqlConnection(_conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "Ins_Cliente_BoosterUsuario";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@p_Nombre", entity.Nombre);
                    cmd.Parameters.AddWithValue("@p_Apellido", entity.Apellido);
                    cmd.Parameters.AddWithValue("@p_Dni", entity.Dni);
                    cmd.Parameters.AddWithValue("@p_Correo", entity.Correo);
                    cmd.Parameters.AddWithValue("@p_Usuario", entity.Usuario);
                    cmd.Parameters.AddWithValue("@p_Clave", entity.Clave);
                    cmd.Parameters.AddWithValue("@p_Alias", entity.Alias);
                    cmd.Parameters.AddWithValue("@p_EsBooster", entity.EsBooster);
                    cmd.Parameters.AddWithValue("@p_IdSteam", entity.IdSteam);

                    cn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        usuarioResponseEntity.codUsuario = Convert.ToInt32(reader["co_cliente"].ToString());
                        usuarioResponseEntity.Alias = reader["tx_Alias"].ToString();
                        usuarioResponseEntity.EsBooster = reader["EsBooster"].ToString();
                        usuarioResponseEntity.Estado = Convert.ToInt16(reader["pEstado"].ToString());
                    }
                    reader.Close();

                }
            }
            return usuarioResponseEntity;
        }

        public int InsBoosterDetalle(EUsuarioDetalleEntity entity)
        {
            int Result = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(_conexion))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "Ins_Usuario_Detalle";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@co_cliente", entity.Codigo);
                        cmd.Parameters.AddWithValue("@no_nombre", entity.Nombre);
                        cmd.Parameters.AddWithValue("@no_Apellido", entity.Apellido);
                        cmd.Parameters.AddWithValue("@nu_Dni", entity.Dni);
                        cmd.Parameters.AddWithValue("@tx_Contrasena", entity.Contraseña);
                        cmd.Parameters.AddWithValue("@co_Steam", entity.IdSteam);
                        cmd.Parameters.AddWithValue("@tx_Telefono", entity.Telefono);
                        cmd.Parameters.AddWithValue("@tx_ubicacion", entity.Ubicacion);
                        cmd.Parameters.AddWithValue("@tx_medalla", entity.medalla);
                        cmd.Parameters.AddWithValue("@tx_Correo", entity.Correo);
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        Result = 1;
                    }

                }

                return Result;
            }
            catch (Exception ex)
            {
                return Result = 0;
            }
        }

        public List<UsuarioResponseEntity> BusquedaBooster()
        {
            List<UsuarioResponseEntity> lista = new List<UsuarioResponseEntity>();

            using (SqlConnection cn = new SqlConnection(_conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "Lis_Booster_detalle";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        lista.Add(
                            new UsuarioResponseEntity()
                            {

                                codUsuario = Convert.ToInt32(reader["Codigo"].ToString()),
                                EsBooster = reader["EsBooster"].ToString(),
                                Nombre = reader["Nombre"].ToString(),
                                Apellido = reader["Apellido"].ToString(),
                                Dni = reader["Dni"].ToString(),
                                CorreoRegistro = reader["CorreoRegistro"].ToString(),
                                Alias = reader["Alias"].ToString(),
                                idSteam = reader["idSteam"].ToString(),
                                Medalla = reader["Medalla"].ToString(),
                                Calificacion = reader["Calificacion"].ToString(),
                                Telefono = reader["Telefono"].ToString(),
                                CorreoContacto = reader["CorreoContacto"].ToString(),
                                Ubicacion = reader["Ubicacion"].ToString()

                            }


                            );

                    }
                }
            }
            return lista;

        }

        public UsuarioResponseEntity BusquedaBoosterById(int id)
        {
            UsuarioResponseEntity lista = new UsuarioResponseEntity();

            using (SqlConnection cn = new SqlConnection(_conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "Lis_Booster_detalle_id";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idUsuario", id);
                    cn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        lista.codUsuario = Convert.ToInt32(reader["Codigo"].ToString());
                        lista.Nombre = reader["Nombre"].ToString();
                        lista.Apellido = reader["Apellido"].ToString();
                        lista.Dni = reader["Dni"].ToString();
                        lista.CorreoRegistro = reader["CorreoRegistro"].ToString();
                        lista.Alias = reader["Alias"].ToString();
                        lista.idSteam = reader["idSteam"].ToString();
                        lista.Medalla = reader["Medalla"].ToString();
                        lista.Calificacion = reader["Calificacion"].ToString();
                        lista.Telefono = reader["Telefono"].ToString();
                        lista.CorreoContacto = reader["CorreoContacto"].ToString();
                        lista.Ubicacion = reader["Ubicacion"].ToString();
                        lista.EsBooster = reader["EsBooster"].ToString();
                    }
                }
            }
            return lista;

        }

    }
}
