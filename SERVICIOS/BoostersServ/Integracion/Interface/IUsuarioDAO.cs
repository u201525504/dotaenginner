﻿using BoostersRestApi.Entidades;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integracion.Interface
{
    public interface IUsuarioDAO
    {
        LoginResponseEntity ObtenerDatos(ELoginEntity entity);
        UsuarioResponseEntity Registro(EUsuarioEntity entity);
        int InsBoosterDetalle(EUsuarioDetalleEntity entity);
        List<UsuarioResponseEntity> BusquedaBooster();
        UsuarioResponseEntity BusquedaBoosterById(int id);


    }
}
