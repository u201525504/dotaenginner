﻿using BoostersRestApi.Data;
using BoostersRestApi.Service;
using BoostersServ.Service;
using Integracion.Interface;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace BoostersRestApi.Controllers.App_Start
{
    public class UnityConfig
    {
        public static UnityContainer RegisterComponents()
        {
           
            var container = new UnityContainer();
            container.RegisterType<IUsuarioService, UsuarioService>();
            container.RegisterType<IUsuarioDAO, UsuarioDAO>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            return container;
        }
    }
}