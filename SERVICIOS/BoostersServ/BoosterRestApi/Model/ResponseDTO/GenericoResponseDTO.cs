﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoostersRestApi.Controllers.Model.ResponseDTO
{
    public class GenericoResponseDTO
    {
        public int Codigo { set; get; }
        public string Resultado { set; get; }
        public string DetalleError { set; get; }
    }
}