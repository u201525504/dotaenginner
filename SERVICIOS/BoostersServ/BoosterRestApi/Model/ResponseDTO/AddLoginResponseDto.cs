﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoostersRestApi.Model
{
    public class AddLoginResponseDto
    {

        public string Codigo { set; get; }
        public string Usuario { set; get; }
        public string Nombre { set; get; }
        public string Apellido { set; get; }
        public string Tipo { set; get; }
    }
}