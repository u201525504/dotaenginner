﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoostersRestApi.Model
{
    public class AddUsuarioResponseDto
    {
        public string Mensaje { set; get; }
        public int Estado { set; get; }
        public int codUsuario { set; get; }
        public string EsBooster { set; get; }
        public string Alias { set; get; }


    }
}