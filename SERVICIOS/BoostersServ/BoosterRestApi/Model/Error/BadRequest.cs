﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoostersRestApi.Controllers.Model.Error
{
    public class BadRequest
    {
        private string typeMessage = "request_error";
        private string message = "La solicitud es invalida.";
        public string Tipo { get { return typeMessage; } }
        public string Mensaje
        {
            set { message = value; }
            get { return message; }
        }

    }
}