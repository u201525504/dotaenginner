﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoostersRestApi.Controllers.Model.RequestDTO
{
    public class AddUsuarioRegistroRequest
    {

        public string Nombre { set; get; }
        public string Apellido { set; get; }
        public string Dni { set; get; }
        public string Correo { set; get; }
        public string Usuario { set; get; }
        public string Clave { set; get; }
        public string Alias { set; get; }
        public string EsBooster { set; get; }
        public string IdSteam { set; get; }
    }
}