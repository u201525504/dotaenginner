﻿
using System.Web.Http;
using BoostersRestApi.Entidades;
using BoostersRestApi.Model;
using BoostersServ.Service;
using BoostersRestApi.Controllers.Model;
using BoostersRestApi.Controllers.Model.Error;
using System.Net;

namespace BoostersRestApi.Controllers
{
    [RoutePrefix("api/v1/login")]
    public class LoginController : ApiController
    {
        private readonly IUsuarioService UsuarioService;

        public LoginController(IUsuarioService UsuarioService)
        {
            this.UsuarioService = UsuarioService;
        }


        //Se envá el usuario y password y devuelve los datos personales de usuario
        [HttpPost]
        public IHttpActionResult Login([FromBody] AddLoginRequest LoginRequest)
        {
            if (LoginRequest == null)
            {
                return Content(HttpStatusCode.BadRequest, new BadRequest());
            }

            LoginResponseEntity entity = UsuarioService.ObtenerDatosLoginRespuesta(parserRequestToDomain(LoginRequest));

            
            return Ok(parserDomainToResponse(entity));

        }

        private ELoginEntity parserRequestToDomain(AddLoginRequest requestData)
        {
            ELoginEntity entity = new ELoginEntity();
            entity.Usuario = requestData.Usuario;
            entity.Password = requestData.Password;
            return entity;
        }

        private AddLoginResponseDto parserDomainToResponse(LoginResponseEntity entity)
        {
            AddLoginResponseDto response = new AddLoginResponseDto();
            response.Codigo = entity.Codigo;
            response.Usuario = entity.Usuario;
            response.Nombre = entity.Nombre;
            response.Apellido = entity.Apellido;
            response.Tipo = entity.Tipo;
            return response;
        }

    }
}