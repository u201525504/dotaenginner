﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using BoostersRestApi.Controllers.Model.RequestDTO;

using BoostersRestApi.Controllers.Model.Error;
using BoostersRestApi.Service;
using BoostersRestApi.Controllers.Model.ResponseDTO;
using BoostersRestApi.Entidades;
using BoostersServ.Service;

namespace BoostersRestApi.Controllers
{
     [RoutePrefix("api/v1/UsuarioRegistro/{id}")]
    public class UsuarioRegistroController : ApiController
    {
         private readonly IUsuarioService UsuarioService;

         public UsuarioRegistroController(IUsuarioService UsuarioService)
        {
            this.UsuarioService = UsuarioService;
        }



        // update detalle
        [HttpPost]
        public IHttpActionResult RegistroDetalle([FromBody] AddUsuarioDetalleRequest LoginRequest)
        {
            int Result = 0;

            if (LoginRequest == null)
            {
                return Content(HttpStatusCode.BadRequest, new BadRequest());
            }

            Result = UsuarioService.BoosterDetalle(parserRequestToDomainDetalle(LoginRequest));

            return Ok(new GenericoResponseDTO()
            {
                Codigo = Result,
                Resultado = "",
                DetalleError = Convert.ToBoolean(Result) ? "Se actualizó con éxito" : "Ocurrió un error"
            });
        }

        private EUsuarioDetalleEntity parserRequestToDomainDetalle(AddUsuarioDetalleRequest requestData)
        {
            EUsuarioDetalleEntity entity = new EUsuarioDetalleEntity();
            entity.Codigo = requestData.Codigo;
            entity.Nombre = requestData.Nombre;
            entity.Apellido = requestData.Apellido;
            entity.Dni = requestData.Dni;
            entity.Contraseña = requestData.Contraseña;
            entity.IdSteam = requestData.IdSteam;
            entity.Telefono = requestData.Telefono;
            entity.Ubicacion = requestData.Ubicacion;
            entity.medalla = requestData.medalla;
            entity.Correo = requestData.Correo;
            return entity;
        }

    }
}