﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Web.Http;
using BoostersServ.Service;
using BoostersRestApi.Controllers.Model.RequestDTO;
using BoostersRestApi.Entidades;
using BoostersRestApi.Controllers.Model.Error;
using BoostersRestApi.Model;
using BoostersRestApi.Controllers.Model.ResponseDTO;

namespace BoostersRestApi.Controllers
{
    [RoutePrefix("api/v1/usuario/{id}")]
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioService UsuarioService;

        public UsuarioController(IUsuarioService UsuarioService)
        {
            this.UsuarioService = UsuarioService;
        }


        //Registro de Usuario/Booster
        [HttpPost]
        public IHttpActionResult Registro([FromBody] AddUsuarioRegistroRequest LoginRequest)
        {

            if (LoginRequest == null)
            {
                return Content(HttpStatusCode.BadRequest, new BadRequest());
            }

            UsuarioResponseEntity entity = UsuarioService.RegistroUsuario(parserRequestToDomain(LoginRequest));

            return Ok(parserDomainToResponse(entity));

        }
        

        // Lista todos los boosters
        [HttpGet]
        public IHttpActionResult BusquedaBooster()
        {
            List<UsuarioResponseEntity> lista = new List<UsuarioResponseEntity>();
            lista = UsuarioService.BusquedaBooster();
            return Ok(lista);
        }

        //Obtiene datos por ID
        [HttpGet]
        public IHttpActionResult BusquedaBoosterById(int id)
        {
            UsuarioResponseEntity lista = new UsuarioResponseEntity();
            lista = UsuarioService.BusquedaBoosterById(id);
            return Ok(lista);
        }

        private AddUsuarioResponseDto parserDomainToResponse(UsuarioResponseEntity entity)
        {
            AddUsuarioResponseDto response = new AddUsuarioResponseDto();
            response.codUsuario = entity.codUsuario;
            response.Alias = entity.Alias;
            response.EsBooster = entity.EsBooster;
            response.Estado = entity.Estado;

            switch (entity.Estado)
            {
                case 1:
                    response.Mensaje = "Se creó el usuario correctamente.";
                    break;
                case    2:
                    response.Mensaje = "El correo ingresado ya se encuentra registrado.";
                    break;
                case 3:
                    response.Mensaje = "El usuario ingresado ya existe.";
                    break;
                case 0:
                    response.Mensaje = "Ocurrió un error durante el registro.";
                    break;
            }
                       
            return response;
        }
        private EUsuarioEntity parserRequestToDomain(AddUsuarioRegistroRequest requestData)
        {
            EUsuarioEntity entity = new EUsuarioEntity();
            entity.Nombre = requestData.Nombre;
            entity.Apellido = requestData.Apellido;
            entity.Dni = requestData.Dni;
            entity.Correo = requestData.Correo;
            entity.Usuario = requestData.Usuario;
            entity.Clave = requestData.Clave;
            entity.Alias = requestData.Alias;
            entity.EsBooster = requestData.EsBooster;
            entity.IdSteam = requestData.IdSteam;
            return entity;
        }
      
    }
}